// /!\ Disclaimer /!\ : Afin de comprendre les principe du map reduce, je tiens a signaler que me suis inspiré du site http://marcautran.developpez.com/tutoriels/javascript/mapreduce/

function mapping() {
	
var input = document.getElementById("inputtxt").value;   //Chaine de caractère d'entrée
	



//Fonction de mapping. Ici, elle sépare les different mots (clés) d'un string rentré en input et les indexe avec une valeur de 1 (valeur).
//Exemple : Pour un input "Hadoop c'est bien très bien", l'output sera "Hadoop,1 , c'est,1 , bien,1 , très,1 , bien,1"
var map = function (inputString) {
    return inputString.split(" ").map(function(inputString) { return [inputString, [1]];});   //Ici la fonction split sépare les different mots et la fonction map créer un index de valeur 1 pour chacun de ces mot
}


//Fonction de classement des données. 
//Etape intermediaire optionnelle entre le mapping et le reducing
//Ici elle regroupe les même mots et ajoute encore une fois un index de valeur 1 a chaque fois que cela se produit. 
//Exemple : Pour un input "Java,1 , Java,1 , Java,1 , Java,1", on obtiendra la sortie Java,1,1,1,1
var shuffle = function (arr) {
  var sortedArray = arr.sort(); //tri par ordre alphabétique des mots /!\ Indispensable pour le futur
  var size = sortedArray.length - 1; //Calcul de la taille (= nombre de mots dans la chaine) du tableau
  
  for(var i = 0 ; i < size; i++) {  //Pour chaque mot, on regarde si celui d'après est le même (fonctionne grace au classement alphabétique)
    if (sortedArray[i][0] == sortedArray[i+1][0]) { 
      sortedArray[i][1].push(1);   // Si c'est le cas on ajoute un nouvel index de valeur 1 avec la méthode push
      sortedArray.splice(i+1, 1);  // Et on n'oublie pas de supprimer l'occurence du mot d'après ! 
      size--;
      i--;
    }
  }
  return sortedArray
}

//Fonction de reducing, fonction finale du process de map/reduce
//Grâce a notre étape de sort/shuffle, elle se contente d'additioner le nombre d'index après un mot (clé) pour en donner le nombre total d'occurence (valeur finale) dans le texte d'entrée
//Exemple : Pour un input "J'aime,1 , le,1,1 , Java,1,1,1,1", on obtient l'output : "J'aime,1 le,2 Java,4"
var reduce = function (arr){
  for (var i = 0; i < shuffledArray.length; i++)  //Pour chaque mot du tableau
  {
    shuffledArray[i][1] = shuffledArray[i][1].reduce(function(a, b) { //On additionne la valeur des index grace a la méthode reduce (ici simplement une addition de 1)
      return a + b;
    });
  }
  return shuffledArray;
}
//Etape 1 : Mapping
var mappedArray = map(input);
console.log(mappedArray); 
document.getElementById("mappingresult").value = mappedArray.join('\n');

//Etape 1.5 : Sorting/Shuffling
var shuffledArray = shuffle(mappedArray);
console.log(shuffledArray);
document.getElementById("sortresult").value = shuffledArray.join('\n');

//Etape 2 : Reducing
var reducedArray = reduce(shuffledArray);
console.log(reducedArray);
document.getElementById("reduceresult").value = reducedArray.join('\n');
}